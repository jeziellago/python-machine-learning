import pandas as pd
from matplotlib import pyplot as plt

# Carrega os dados do arquivo .csv
dataset = pd.read_csv('Iris.csv')

# Lambda para filtrar o dataset por espécie
filter_by_specie = lambda specie : dataset.query('Species == "%s"' % specie)

# Dataset separado por espécie
dataset_virginica = filter_by_specie('Iris-virginica')
dataset_versicolor = filter_by_specie('Iris-versicolor')
dataset_setosa = filter_by_specie('Iris-setosa')

def plot_dataset(key):
    plt.plot(dataset_virginica[key], 'ro', label='Virginica')
    plt.plot(dataset_versicolor[key], 'bx', label='Versicolor')
    plt.plot(dataset_setosa[key], 'g*', label='Setosa')
    plt.xlabel('Amostra')
    plt.ylabel(key)
    plt.legend()
    plt.show()
    plt.close()

if __name__ == '__main__':
    plot_dataset('PetalLengthCm')
