import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

logistic_regression = LogisticRegression()

def read_csv():
    dataset = pd.read_csv('Iris.csv')
    factors = pd.factorize(pd.Series(dataset['Species']))
    dataset['Response'] = factors[0]

    return dataset

def split_dataset(dataset):
    train, test = train_test_split(dataset, test_size=0.2)
    X_train, y_train = split_observation_response(train)
    X_test, y_test = split_observation_response(test)

    return X_train, y_train, X_test, y_test

def split_observation_response(dataframe):
    return dataframe[['PetalLengthCm', 'PetalWidthCm']], dataframe['Response']

def train_model(X_train, y_train):
    logistic_regression.fit(X_train, y_train)

def test_model(X_test, y_test):
    y_predicted = logistic_regression.predict(X_test)
    
    hitPercentage = accuracy_score(y_test, y_predicted)
    hitNumber = accuracy_score(y_test, y_predicted, False)
    print("Accuracy of %.6f%%, correctly classified %d observations." % ((hitPercentage * 100), hitNumber))

if __name__ == '__main__':
    dataset = read_csv()
    
    X_train, y_train, X_test, y_test = split_dataset(dataset)
    train_model(X_train, y_train)
    test_model(X_test, y_test)